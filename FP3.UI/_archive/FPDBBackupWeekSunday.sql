IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Positions]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Locations]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_UserDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_UserDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_OpenPositions]
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 9/15/2019 5:15:33 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
DROP TABLE [dbo].[UserDetails]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 9/15/2019 5:15:33 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
DROP TABLE [dbo].[Positions]
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 9/15/2019 5:15:33 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
DROP TABLE [dbo].[OpenPositions]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 9/15/2019 5:15:33 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
DROP TABLE [dbo].[Locations]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 9/15/2019 5:15:33 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
DROP TABLE [dbo].[Applications]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 9/15/2019 5:15:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Applications](
	[ApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[OpenPositionID] [int] NOT NULL,
	[UserID] [nvarchar](128) NOT NULL,
	[ApplicationDate] [date] NOT NULL,
	[ManagerNotes] [varchar](2000) NULL,
	[IsDeclined] [bit] NOT NULL,
	[ResumeFilename] [varchar](75) NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 9/15/2019 5:15:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Locations](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[StoreNumber] [varchar](10) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[ManagerID] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 9/15/2019 5:15:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpenPositions](
	[OpenPositionID] [int] IDENTITY(1,1) NOT NULL,
	[PositionID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
 CONSTRAINT [PK_OpenPositions] PRIMARY KEY CLUSTERED 
(
	[OpenPositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 9/15/2019 5:15:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Positions](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[JobDescription] [varchar](max) NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 9/15/2019 5:15:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserDetails](
	[UserID] [nvarchar](128) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[ResumeFIlename] [varchar](75) NOT NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Applications] ON 

INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (5, 4, N'eda278e2-1ecb-4c21-b70b-c438c89934ac', CAST(0x20400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (9, 5, N'b9d3079b-b062-4e13-9d47-7630701dc541', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (10, 5, N'b9d3079b-b062-4e13-9d47-7630701dc541', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (11, 4, N'b9d3079b-b062-4e13-9d47-7630701dc541', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (12, 4, N'b9d3079b-b062-4e13-9d47-7630701dc541', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (13, 4, N'455251cf-b24c-4fd2-997d-2f270c52749b', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (14, 6, N'455251cf-b24c-4fd2-997d-2f270c52749b', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (15, 4, N'455251cf-b24c-4fd2-997d-2f270c52749b', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (16, 5, N'455251cf-b24c-4fd2-997d-2f270c52749b', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (17, 4, N'455251cf-b24c-4fd2-997d-2f270c52749b', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (18, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (19, 3, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (20, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (21, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (22, 5, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (23, 6, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (24, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (25, 9, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (26, 9, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (27, 5, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (28, 9, N'cd2713df-a037-46fb-8872-d9ab522d9726', CAST(0x21400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (29, 9, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x21400B00 AS Date), N'Hey Dane, We would like to have you in for an interview soon!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (30, 9, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x22400B00 AS Date), N'Hey Dane, We would like to have you in for an interview soon!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (31, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x22400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (32, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x22400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (33, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x22400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (34, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x24400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (35, 4, N'151a945c-5e08-436d-bac9-3ffbdd5b601f', CAST(0x24400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
SET IDENTITY_INSERT [dbo].[Applications] OFF
SET IDENTITY_INSERT [dbo].[Locations] ON 

INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (1, N'1', N'Overland Park', N'KS', N'2')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (2, N'2', N'Beverly Hills', N'CA', N'6')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (3, N'3', N'Leawood', N'KS', N'4')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (4, N'5', N'Topeka', N'KS', N'5')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (5, N'16', N'Chicago', N'IL', N'2')
SET IDENTITY_INSERT [dbo].[Locations] OFF
SET IDENTITY_INSERT [dbo].[OpenPositions] ON 

INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (3, 2, 2)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (4, 2, 4)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (5, 3, 2)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (6, 5, 3)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (7, 2, 3)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (9, 7, 5)
SET IDENTITY_INSERT [dbo].[OpenPositions] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (2, N'Jr. Software Developer', N'Create Web Applications using the .Net framework. Must have 1 year of experience in CSS, HTML, and JavaScript. Must be a hard-worker and willing to learn. Apply now!')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (3, N'CEO', N'Be the man in charge ')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (5, N'Sr. Software Developer', N'We are currently looking for a Sr. Software Developer with 3+ years of experience using the .Net Framework. Must be proficient in HTML, CSS, JavaScript, C#, SQL and must be a good teamworker. Apply now!')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (6, N'Head Cheese of fixing stuff', N'Must be wiling to do whatever it takes to get job done')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (7, N'JavaScript Developer', N'We need a JavaScript Developer that has at least 1 year of experience')
SET IDENTITY_INSERT [dbo].[Positions] OFF
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'151a945c-5e08-436d-bac9-3ffbdd5b601f', N'Test', N'User', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'3794bb3a-e965-4bec-930a-1b51a294e1bb', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'455251cf-b24c-4fd2-997d-2f270c52749b', N'dane', N'dane', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'4baa8ecc-b617-4e14-bd5b-d3b99da6a91b', N'test', N'testy', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'527fd29e-a308-44b1-8d39-19722a8c6ece', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'ab510e41-5003-47b0-b5bb-9754e9593cb4', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'b9d3079b-b062-4e13-9d47-7630701dc541', N'Bruce ', N'Wayne', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'cd2713df-a037-46fb-8872-d9ab522d9726', N'New', N'Guy', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'eda278e2-1ecb-4c21-b70b-c438c89934ac', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_OpenPositions] FOREIGN KEY([OpenPositionID])
REFERENCES [dbo].[OpenPositions] ([OpenPositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_OpenPositions]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_UserDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_UserDetails] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetails] ([UserID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_UserDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_UserDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Locations] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Locations]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Positions] FOREIGN KEY([PositionID])
REFERENCES [dbo].[Positions] ([PositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Positions]
GO
