IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Positions]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Locations]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_UserDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_UserDetails]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_OpenPositions]
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
DROP TABLE [dbo].[UserDetails]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
DROP TABLE [dbo].[Positions]
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
DROP TABLE [dbo].[OpenPositions]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
DROP TABLE [dbo].[Locations]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
DROP TABLE [dbo].[Applications]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 9/17/2019 10:39:12 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[__MigrationHistory]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Applications](
	[ApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[OpenPositionID] [int] NOT NULL,
	[UserID] [nvarchar](128) NOT NULL,
	[ApplicationDate] [date] NOT NULL,
	[ManagerNotes] [varchar](2000) NULL,
	[IsDeclined] [bit] NOT NULL,
	[ResumeFilename] [varchar](75) NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Locations](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[StoreNumber] [varchar](10) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[ManagerID] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpenPositions](
	[OpenPositionID] [int] IDENTITY(1,1) NOT NULL,
	[PositionID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
 CONSTRAINT [PK_OpenPositions] PRIMARY KEY CLUSTERED 
(
	[OpenPositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Positions](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[JobDescription] [varchar](max) NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 9/17/2019 10:39:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserDetails](
	[UserID] [nvarchar](128) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[ResumeFIlename] [varchar](75) NOT NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201907171958314_InitialCreate', N'FP2.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5C5B6FE3B6127E3FC0F90F829E7A0E522B71CE2EB681BD8BD4897B826E2E58678BBE2D68897684952855A2D204457F591FFA93FA173A94A81B2FBAD88AED14058AB538FC66381C92C3D1A7FCF5C79F930F4FBE673CE228760332354F46C7A681891D382E594FCD84AEBE7D677E78FFEF7F4D2E1DFFC9F829973B6572D093C453F381D2F0CCB262FB01FB281EF9AE1D0571B0A2233BF02DE404D6F8F8F83BEBE4C4C200610296614C3E2584BA3E4E7FC0CF59406C1CD20479D78183BD983F8796458A6ADC201FC721B2F1D49CDF8D479994699C7B2E020B16D85B99062224A088827D679F63BCA05140D68B101E20EFFE39C420B7425E8CB9DD67A578D7211C8FD910ACB2630E6527310DFC9E8027A7DC2796D87D23CF9A85CFC06B97E05DFACC469D7A6E6A5E39387DF429F0C001A2C2B3991731E1A9795DA8388FC31B4C4779C75106398F00EED720FA3AAA221E199DFB1D1531341E1DB3FF8E8C59E2D124C25382131A21EFC8B84B969E6BFF889FEF83AF984C4F4F96ABD3776FDE22E7F4EDFFF0E99BEA4861AC20577B008FEEA220C411D88657C5F84DC3AAF7B3C48E45B74A9FCC2B104BB01C4CE31A3D7DC4644D1F60A18CDF99C6DC7DC24EFE8407D767E2C2EA814E344AE0E74DE27968E9E1A2DD6AD4C9FEDFA075FCE6ED205A6FD0A3BB4EA75ED00F0B278275F5097B696BFCE086D9F2AACDF7172E368F029FFDAEC757D6FA65112491CD06136845EE51B4C6B46EDDC42A83B7534833A8E1C33A473DFCD06696CAE1AD146503DA6425E42A76BD1A727B5F566FE7883B0F4398BC34B498479A02AE3CA44642AF2303DACA5039E91A2A0486F04FDEF92E7DE47A036C7D1DB440B6B172231F17A3FC3E804043A4B7CD77288E61E53BFF47F14383E9F0CF014C5F603B8920201714F9E18B6BBB7B0808BE49FC258BF3DDE91A6C6AEE7F0DE6C8A641744958AFADF13E06F6D720A197C4B940147FA6760EC87EDEBB7E778041CC39B76D1CC7730866ECCC0248A673C02B424FC7BDE1D8C6B4EFD463E621D757E71EC216FA25172DF30FB584948368C454794893A91F83B54BBA999A8BEA4DCD245A4DE5627D4D6560DD2CE5927A435381563B33A9C132BB7486864FED52D8C3CFEDB63BBC757B41C58D0BD821F10F98E008B631E70E518A2352CE40977D631FC9423A7D4CE98B9F4DA9A69F90970CAD6AA3D5906E02C3AF8614F6F057436A263C7E741D969574B8F0E4C200DF495E7D976A5F738265BB5E0EB561EE5AF96EF600DD72398FE3C076D355A02875F14245DD7EC8E18CF6AA45361AB1F20103834077D991074F606CA61854B7E4027B9862E3DCCE4A813314DBC891DD0803727A18969FA80AC3CA0A48DDB8FF4A3A21D271C43A2176098A61A5BA84CACBC225B61B22AFD54B42CF8E47181B7BA1436CB9C021264C61AB27BA2857173C9801851E6152DA3C34B12A11D71C889AAC5537E76D296C39EF521D622731D9923B6BE292E76F2F1298CD1EDB417036BBA48B01DAE2DD3E0294DF55BA0680787139B400156E4C9A00E529D54E02B4EEB13D0468DD25AF2E40B32B6AD7F917EEAB87169EF58BF2EE8FF54677ED21366BFE38B0D0CC724FE843A1078EE4F0BC58B246FC44159733B093DFCF629EEA8A21C2C01798D64B3665BEABCC43AD661031889A00CB406B01E5AFFD24206941F5302EAFE5355AC7B3881EB079DDAD1196EFFD026C250664ECEAEBCF8AA0FE25A9189C9D6E1FC5C88A689082BCD365A182A3080871F3AA0FBC8353747559D9315D72E13ED97065607C321A1CD492B96A9C940F66702FE5A1D9EE255542D62725DBCA4B42FAA4F1523E98C1BDC463B4DD498AA4A0475AB0958BEA47F8408B2DAF7414A74DD136B13236147F30B134B4A9C9350A4397AC2B342AFEC458641CAAD9B78BFE24233FC3B0EC58C1352AAC2D34D120426B2CB4826AB074EE4631BD40142D11ABF3CC1C5F12539EAD9AED3F57593D3EE549CCCF815C9AFD3BEB5179595F3B63E52484F79DC3C87C96C9A4E573C5BCABBB1B8CCE863C14292AF6B3C04B7CA24FACF4BDB3F776D5FED91319616209F64B8993E42529BDADBBBCD384C88B61CBC929F295CD27480FA173739E6D561DADCB40F5287941AA8AA22B52ED6DC274894BA7491273C1FE73D48AF032EB881350AA00FC514F8C0A874102ABB47547ADD34CAA98F596EE880297A40A2934F5B0B2CA18A919596DD8084FE351B544770D3247A48A2EB7764756B045AAD08AE60DB015368B6DDD511584922AB0A2B93B76C92E1137CF033EA9B43794DE47557679DDEEACD260BCCC4E38CC515779475F05AA3CEE89C5DFC24B60FCF9414691F606D73B8AB25AC57651A4C1D0EF34B5B7DAF58DA6F155BC1EB3F6AABAB69937BDAAD7E3F58BD5178D08E9E2268A14DA8B0B9C70519BF04B53FB4730D22D2A13318DDC8D70903FC714FB2326305AFCE2CD3C17B36D3B17B846C45DE19866F40C737C7C3216BEA7399C6F5BAC38763CC5A553F7814B7DCE76C0B4228F28B21F5024F31EB6F8FEA304954ACA57C4C14F53F3B7B4D7595A9D60FF4A1F1F1957F167E2FE9240C37D9460E37799C7390C1FBEF93275A05F2F74F7EAD5CF5FB2AE47C66D042BE6CC38167CB9C90CD7BF69E8654DD6750B6B36FED2E1F52EA8DA67054A5461416CFE15C1D2A5837C41905BF98D8F9EFED3D734E557025B212ABE04180A6F1017EA98FE9B606959FE0EFCA429CBBFDF60D5ACFF4D4CD332FE5DD21F4CE4FB77DF86F29E7B3C6A1477A15D6C49A99F5BF9D25B9127F77D3649B4EAAD16BA4C9DEE01B7053D7A83C87865CCE2C14E4705717830EC7D86F68BB3850F85205C5237F6CB0BDE2515B8E1F5CF3F8A017C009C35050767FF3CDF5DC79AAE7E7BE064C97E6CDE030B36CECCDA3F6777D7C1A62BF31E78B0F562E61E58ACEDEBFCDC73A4753E42F7CEB39529439AF730AA5A701B8F362B9CC30D7F194010641965F6F9A39AB8D5443A6D51588AE895EA1963A26269E1487A258966B5FDC6CA0FFCC6C1729966B51A9E65936EBEFF37EAE632CDBA35ECC57D308095FC41152BBB651F6B223BBD26C66F6D242D04F3B69CB5F1A5FA6B22F80EE294DAEAD1BC237E3D7CDE415C32E4D2E9C1DF955FF7C2D959F9CB88707EC7EEBA84607F279160BB766A1632576415E487B760512E225468AE31450E1CA9E7117557C8A6D0CC6ACCE9F7DB69DD8EBDE95862E78ADC26344C280C19FB4BAF56F062494093FE94A45CB779721BA67F8A64882180992EABCDDF92EF13D7730ABBE78A9A9006826517BCA2CBE692B2CAEEFAB940BA09484720EEBE2229BAC77EE801587C4B16E8116F621B84DF47BC46F6735901D481B44F44DDED930B17AD23E4C71CA3EC0F3F21861DFFE9FDDFFDFD60BC20540000, N'6.1.3-40302')
SET IDENTITY_INSERT [dbo].[Applications] ON 

INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (36, 13, N'079233a8-5fc9-4618-8188-157817dfe7c2', CAST(0x24400B00 AS Date), N'Hey, welcome to the team!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (37, 12, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (38, 14, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), N'Hey dane, we would love to have you in for an interview! see you soon', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (39, 15, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), N'Hey Dane, you seem like a great candidate. Let''s set up and interview!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (40, 13, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (41, 17, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (42, 19, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (43, 19, N'26e55662-2054-4e87-93b4-38f7602562d0', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (44, 12, N'26e55662-2054-4e87-93b4-38f7602562d0', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (45, 11, N'26e55662-2054-4e87-93b4-38f7602562d0', CAST(0x25400B00 AS Date), N'Hey Dane, We would like to have you in for an interview soon!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (46, 18, N'289c6914-ab5d-490e-a831-168060bcb71d', CAST(0x25400B00 AS Date), N'Hey Dane, We would like to have you in for an interview soon!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (47, 11, N'8f5e99ce-d905-4f9e-a072-a53246d47aa0', CAST(0x25400B00 AS Date), N'hey whats up', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (48, 17, N'2812ebe1-ed49-4784-a727-aab86259b058', CAST(0x25400B00 AS Date), N'Hey Dane! I would love to get in contact with you!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (49, 13, N'3092fa8c-3142-4153-a472-5bfa4d554332', CAST(0x25400B00 AS Date), N'Hey Dane, We would like to have you in for an interview soon!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (50, 11, N'3092fa8c-3142-4153-a472-5bfa4d554332', CAST(0x25400B00 AS Date), N'Hey Dane, We would like to have you in for an interview soon!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (51, 11, N'3092fa8c-3142-4153-a472-5bfa4d554332', CAST(0x25400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (52, 18, N'1e0573d5-7d0b-46e6-85a0-2ca229ccbd52', CAST(0x25400B00 AS Date), N'Hey Dane! You seem like a great fit for our company! Lets get in touch', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (53, 19, N'1e0573d5-7d0b-46e6-85a0-2ca229ccbd52', CAST(0x25400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (54, 17, N'1e0573d5-7d0b-46e6-85a0-2ca229ccbd52', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (55, 17, N'1e0573d5-7d0b-46e6-85a0-2ca229ccbd52', CAST(0x26400B00 AS Date), N'Yes', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (56, 11, N'1e0573d5-7d0b-46e6-85a0-2ca229ccbd52', CAST(0x26400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
SET IDENTITY_INSERT [dbo].[Applications] OFF
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'06e2df1d-0a27-4549-94d7-498d110ef09f', N'Admin')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'22201614-f4a6-4924-9da9-ef53d91faf6c', N'06e2df1d-0a27-4549-94d7-498d110ef09f')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'22201614-f4a6-4924-9da9-ef53d91faf6c', N'admin@example.com', 0, N'AL8SCUoDK95t5soKeIkBaDtoJgyX9Cy5cDPNvt75gN46m9cYl3Clr4Q/dow2YJhR8w==', N'3af337b5-5b43-489f-a637-fc9a35a60130', NULL, 0, 0, NULL, 0, 0, N'admin@example.com')
SET IDENTITY_INSERT [dbo].[Locations] ON 

INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (10, N'3', N'Beverly Hills', N'CA', N'87b4d416-7e9d-46e0-a3bf-6842d7dd3e48')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (11, N'1', N'Overland Park', N'KS', N'29002687-40ab-48ff-b723-baf972da6aa5')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (12, N'2', N'Kansas City', N'MO', N'29002687-40ab-48ff-b723-baf972da6aa5')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (13, N'4', N'New York', N'NY', N'87b4d416-7e9d-46e0-a3bf-6842d7dd3e48')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (15, N'5', N'Austin', N'TX', N'73c67e9c-be17-40f8-a4e4-450ffaea651b')
SET IDENTITY_INSERT [dbo].[Locations] OFF
SET IDENTITY_INSERT [dbo].[OpenPositions] ON 

INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (11, 10, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (12, 9, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (13, 11, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (14, 10, 10)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (15, 9, 13)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (16, 12, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (17, 13, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (18, 14, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (19, 16, 15)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (20, 17, 11)
SET IDENTITY_INSERT [dbo].[OpenPositions] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (9, N'Sr. Software Developer', N'Create Web Applications using the .Net framework. Must have 3+ years of experience in the .Net stack')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (10, N'Jr. Software Developer', N'Create Web Applications using the .Net framework. Must have 1 year of experience in CSS, HTML, and JavaScript. Must be a hard-worker and willing to learn. Apply now!')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (11, N'JavaScript Developer', N'Must have 3+ years of experience with JavaScript and jQuery. ')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (12, N'Business Analyst', N'Helps our business by improving processes, products, services and software through data analysis. 3 years of relevant business experience preferred.')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (13, N'CEO', N'We are currently looking for a CEO for our multi-billion dollar company. Apply now!')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (14, N'Database Developer', N'Currently looking for a database developer. Must be familiar with PostgreSQL and have 3 years of relevant experience with database development.')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (15, N'TESTJOB', N'TESTING')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (16, N'React Developer', N'We need a React developer with multiple years of experience. Apply Now!')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (17, N'Front End Developer', N'Work with html and css to create web pages')
SET IDENTITY_INSERT [dbo].[Positions] OFF
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'079233a8-5fc9-4618-8188-157817dfe7c2', N'Dane ', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'1e0573d5-7d0b-46e6-85a0-2ca229ccbd52', N'Dave', N'Pendersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'26e55662-2054-4e87-93b4-38f7602562d0', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'2812ebe1-ed49-4784-a727-aab86259b058', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'289c6914-ab5d-490e-a831-168060bcb71d', N'Dane K', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'3092fa8c-3142-4153-a472-5bfa4d554332', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'63686b3c-74dc-477d-b423-97868f33a1f4', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'73c67e9c-be17-40f8-a4e4-450ffaea651b', N'Manager', N'Three', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'87b4d416-7e9d-46e0-a3bf-6842d7dd3e48', N'Manager', N'Two', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'8f5e99ce-d905-4f9e-a072-a53246d47aa0', N'Dane K', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'd7135960-42de-43f0-9413-1c0ddf55e1a6', N'Manager', N'Two', N'DanePedersenResumeIT.pdf')
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_OpenPositions] FOREIGN KEY([OpenPositionID])
REFERENCES [dbo].[OpenPositions] ([OpenPositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_OpenPositions]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_UserDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_UserDetails] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetails] ([UserID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_UserDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_UserDetails]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Locations] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Locations]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Positions] FOREIGN KEY([PositionID])
REFERENCES [dbo].[Positions] ([PositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Positions]
GO
