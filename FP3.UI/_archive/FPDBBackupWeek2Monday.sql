ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Positions]
GO
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Locations]
GO
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_UserDetails]
GO
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_OpenPositions]
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 9/16/2019 1:59:20 PM ******/
DROP TABLE [dbo].[UserDetails]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 9/16/2019 1:59:20 PM ******/
DROP TABLE [dbo].[Positions]
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 9/16/2019 1:59:20 PM ******/
DROP TABLE [dbo].[OpenPositions]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 9/16/2019 1:59:20 PM ******/
DROP TABLE [dbo].[Locations]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 9/16/2019 1:59:20 PM ******/
DROP TABLE [dbo].[Applications]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 9/16/2019 1:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Applications](
	[ApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[OpenPositionID] [int] NOT NULL,
	[UserID] [nvarchar](128) NOT NULL,
	[ApplicationDate] [date] NOT NULL,
	[ManagerNotes] [varchar](2000) NULL,
	[IsDeclined] [bit] NOT NULL,
	[ResumeFilename] [varchar](75) NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 9/16/2019 1:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Locations](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[StoreNumber] [varchar](10) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[ManagerID] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 9/16/2019 1:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpenPositions](
	[OpenPositionID] [int] IDENTITY(1,1) NOT NULL,
	[PositionID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
 CONSTRAINT [PK_OpenPositions] PRIMARY KEY CLUSTERED 
(
	[OpenPositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Positions]    Script Date: 9/16/2019 1:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Positions](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[JobDescription] [varchar](max) NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 9/16/2019 1:59:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserDetails](
	[UserID] [nvarchar](128) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[ResumeFIlename] [varchar](75) NOT NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Applications] ON 

INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (36, 13, N'079233a8-5fc9-4618-8188-157817dfe7c2', CAST(0x24400B00 AS Date), N'Hey, welcome to the team!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (37, 12, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (38, 14, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), N'Hey dane, we would love to have you in for an interview! see you soon', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (39, 15, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), N'Hey Dane, you seem like a great candidate. Let''s set up and interview!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (40, 13, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (41, 17, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (42, 19, N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (43, 19, N'26e55662-2054-4e87-93b4-38f7602562d0', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (44, 12, N'26e55662-2054-4e87-93b4-38f7602562d0', CAST(0x25400B00 AS Date), NULL, 1, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (45, 11, N'26e55662-2054-4e87-93b4-38f7602562d0', CAST(0x25400B00 AS Date), N'Hey Dane, We would like to have you in for an interview soon!', 0, N'DanePedersenResumeIT.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (46, 18, N'289c6914-ab5d-490e-a831-168060bcb71d', CAST(0x25400B00 AS Date), NULL, 0, N'DanePedersenResumeIT.pdf')
SET IDENTITY_INSERT [dbo].[Applications] OFF
SET IDENTITY_INSERT [dbo].[Locations] ON 

INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (10, N'3', N'Beverly Hills', N'CA', N'87b4d416-7e9d-46e0-a3bf-6842d7dd3e48')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (11, N'1', N'Overland Park', N'KS', N'29002687-40ab-48ff-b723-baf972da6aa5')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (12, N'2', N'Kansas City', N'MO', N'29002687-40ab-48ff-b723-baf972da6aa5')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (13, N'4', N'New York', N'NY', N'87b4d416-7e9d-46e0-a3bf-6842d7dd3e48')
INSERT [dbo].[Locations] ([LocationID], [StoreNumber], [City], [State], [ManagerID]) VALUES (15, N'5', N'Austin', N'TX', N'73c67e9c-be17-40f8-a4e4-450ffaea651b')
SET IDENTITY_INSERT [dbo].[Locations] OFF
SET IDENTITY_INSERT [dbo].[OpenPositions] ON 

INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (11, 10, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (12, 9, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (13, 11, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (14, 10, 10)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (15, 9, 13)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (16, 12, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (17, 13, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (18, 14, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (19, 16, 15)
SET IDENTITY_INSERT [dbo].[OpenPositions] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (9, N'Sr. Software Developer', N'Create Web Applications using the .Net framework. Must have 3+ years of experience in the .Net stack')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (10, N'Jr. Software Developer', N'Create Web Applications using the .Net framework. Must have 1 year of experience in CSS, HTML, and JavaScript. Must be a hard-worker and willing to learn. Apply now!')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (11, N'JavaScript Developer', N'Must have 3+ years of experience with JavaScript and jQuery. ')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (12, N'Business Analyst', N'Helps our business by improving processes, products, services and software through data analysis. 3 years of relevant business experience preferred.')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (13, N'CEO', N'We are currently looking for a CEO for our multi-billion dollar company. Apply now!')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (14, N'Database Developer', N'Currently looking for a database developer. Must be familiar with PostgreSQL and have 3 years of relevant experience with database development.')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (15, N'TESTJOB', N'TESTING')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (16, N'React Developer', N'We need a React developer with multiple years of experience. Apply Now!')
SET IDENTITY_INSERT [dbo].[Positions] OFF
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'079233a8-5fc9-4618-8188-157817dfe7c2', N'Dane ', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'26e55662-2054-4e87-93b4-38f7602562d0', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'289c6914-ab5d-490e-a831-168060bcb71d', N'Dane K', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'63686b3c-74dc-477d-b423-97868f33a1f4', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'6baef7dd-f2f3-4e4e-95f9-dd3356e68a27', N'Dane', N'Pedersen', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'73c67e9c-be17-40f8-a4e4-450ffaea651b', N'Manager', N'Three', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'87b4d416-7e9d-46e0-a3bf-6842d7dd3e48', N'Manager', N'Two', N'DanePedersenResumeIT.pdf')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFIlename]) VALUES (N'd7135960-42de-43f0-9413-1c0ddf55e1a6', N'Manager', N'Two', N'DanePedersenResumeIT.pdf')
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_OpenPositions] FOREIGN KEY([OpenPositionID])
REFERENCES [dbo].[OpenPositions] ([OpenPositionID])
GO
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_OpenPositions]
GO
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_UserDetails] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetails] ([UserID])
GO
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_UserDetails]
GO
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Locations] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Locations]
GO
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Positions] FOREIGN KEY([PositionID])
REFERENCES [dbo].[Positions] ([PositionID])
GO
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Positions]
GO
