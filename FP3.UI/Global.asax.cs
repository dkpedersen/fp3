﻿using FP3.UI.Models;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FP3.UI
{
    // Note: For instructions on enabling IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=301868
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error()
        {
            //quick fix error handling site-wide: take them to generic page.
           // Response.Redirect("/Error.html");

            //We'll disable this "custom error page" while doing local dev & test
            //but it would be SWELL to uncomment/activate the Response.Redirect above
            //right before publish/deploy.
        }
    }
}
