﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FP3.Data.EF;
using FP3.UI.Models;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;

namespace FP3.UI.Controllers
{
    [Authorize]
    public class ApplicationsController : Controller
    {
        private FinalProjectEntities db = new FinalProjectEntities();

        // GET: Applications


        public ActionResult Index()
        {
            //get managerid
            var manager = User.Identity.GetUserId().ToString();
            //get user details
            ViewBag.user = db.UserDetails.AsEnumerable();
            //get applications
            ViewBag.application = db.Applications.Include(a => a.OpenPosition.Position);

            ViewBag.Man = User.Identity.GetUserId().ToString();
            ViewBag.count = db.Applications.Where(x=>x.OpenPosition.Location.ManagerID.ToLower().ToString() == manager.ToLower().ToString() && x.IsDeclined == false && x.ManagerNotes == null).Count();

            var application = db.Applications.Include(a => a.OpenPosition.Position);

          
      
            return View(application.ToList());


        }

        // GET: Applications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            
            

            return View(application);
        }

        // GET: Applications/Create
        public ActionResult Create()
        {
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID");
            ViewBag.ApplicationID = new SelectList(db.Applications, "ApplicationID", "ApplicationID");

            UserDetail ud = db.UserDetails.Find(User);
            OpenPosition op = new OpenPosition();
            Application app = new Application();


            app.ApplicationDate = DateTime.Now;
            app.UserID = ud.UserID;
            app.IsDeclined = false;
            app.ManagerNotes = null;
            app.ResumeFilename = ud.ResumeFIlename;
            db.Applications.Add(app);
            db.SaveChanges();


            return Json(app);
        }

        // POST: Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Application application, HttpPostedFileBase resume)
        {



            if (ModelState.IsValid)
            {


                ApplicationViewModel rv = new ApplicationViewModel();
                rv.ApplicationID = ViewBag.ApplicationID;
                //rv.UserID = ;
                rv.ApplicationDate = DateTime.Now;
                rv.ManagerNotes = application.ManagerNotes;
                rv.IsDeclined = application.IsDeclined;
                rv.ResumeFilename = application.ResumeFilename;
                db.Applications.Add(application);
                db.SaveChanges();
                return RedirectToAction("Pending");
            }

            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // GET: Applications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // GET: Applications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Application application = db.Applications.Find(id);
            db.Applications.Remove(application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Pending()
        {
            //get the users id
            var user = User.Identity.GetUserId();
            //Get the jobs for this specific user
            ViewBag.Pending = db.Applications.Where(x => x.IsDeclined != true && x.UserID == user && x.ManagerNotes == null);
            ViewBag.NoJob = db.Applications.Where(x => x.IsDeclined == true && x.UserID == user);
            ViewBag.Job = db.Applications.Where(x => x.ManagerNotes != null && x.UserID == user);

            //Count the jobs for this specific user
            ViewBag.JobCount = db.Applications.Where(x => x.ManagerNotes != null && x.UserID == user).Count();
            ViewBag.NoJobCount = db.Applications.Where(x => x.IsDeclined == true && x.UserID == user).Count();            
            ViewBag.PendingCount = db.Applications.Where(x => x.IsDeclined != true && x.UserID == user && x.ManagerNotes == null).Count();
            var userApp = db.Applications.Where(x => x.UserID == user);                        
            return View(userApp);
        }
    }
}
