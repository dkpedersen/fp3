﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FP3.Data.EF;
using FP3.UI.Models;
using Microsoft.AspNet.Identity;

namespace FP3.UI.Controllers
{
    [Authorize]
    public class OpenPositionsController : Controller
    {
        private FinalProjectEntities db = new FinalProjectEntities();

        // GET: OpenPositions
        public ActionResult Index()
        {
            //get all open positions
            var openPositions = db.OpenPositions.Include(o => o.Location).Include(o => o.Position); 
            //get the users id
            var user = User.Identity.GetUserId();
            //The manager has not accepted or declined
            ViewBag.Pending = db.Applications.Where(x => x.IsDeclined != true && x.UserID == user && x.ManagerNotes == null);
            //The manager has declined
            ViewBag.NoJob = db.Applications.Where(x => x.IsDeclined == true && x.UserID == user);
            //The manager has reached out to the applicant
            ViewBag.Job = db.Applications.Where(x => x.ManagerNotes != null && x.UserID == user );
            //Get jobs specific to the managers locations
            ViewBag.manager = db.OpenPositions.Where(x => x.Location.ManagerID == user);
            //Return jobs where the applicant has not yet applied. Used for when they are coming back to the page
            var Noapply = db.Applications.Where(x => x.UserID == user).Select(x => x.OpenPositionID).ToString();
            //Return noapply viewbag
            ViewBag.NoApply = db.OpenPositions.Where(x => x.OpenPositionID.ToString() != Noapply);
            //Count the jobs for this specific user
            ViewBag.JobCount = db.Applications.Where(x => x.ManagerNotes != null && x.UserID == user).Count();
            //Count the declined jobs for this specific user
            ViewBag.NoJobCount = db.Applications.Where(x => x.IsDeclined == true && x.UserID == user).Count();
            //Count the pending jobs for this specific user
            ViewBag.PendingCount = db.Applications.Where(x => x.IsDeclined != true && x.UserID == user && x.ManagerNotes == null).Count();
            //applications the user has submitted
            var userApp = db.Applications.Where(x => x.UserID == user);           
            return View(openPositions.ToList());
                      
        }

        //var Pending = db.Applications.Where(x => x.IsDeclined != true && x.UserID == user && x.ManagerNotes == null).Select(x => x.OpenPositionID).ToString();
        //var NoJob = db.Applications.Where(x => x.IsDeclined == true && x.UserID == user).Select(x => x.OpenPositionID).ToString();
        //var Job = db.Applications.Where(x => x.ManagerNotes != null && x.UserID == user).Select(x => x.OpenPositionID).ToString();
        //ViewBag.NoApply = db.OpenPositions.Where(x => x.OpenPositionID.ToString() != Job && x.OpenPositionID.ToString() != NoJob && x.OpenPositionID.ToString() != Pending && x.OpenPositionID.ToString() != Noapply);

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult AjaxCreate(Application apply)
        //{


        //}
        [HttpGet]
        public ActionResult Application(int app)
        {
            string user = User.Identity.GetUserId();
           UserDetail ud = db.UserDetails.Find(user);
            Application apple = new Application();

            apple.OpenPositionID = app;
            apple.UserID = ud.UserID;        
            apple.ApplicationDate = DateTime.Now;
            apple.ResumeFilename = ud.ResumeFIlename;
            db.Applications.Add(apple);
            db.SaveChanges();
            ViewBag.Message = "Thanks for submitting an application. Check back soon for updates.";
            return RedirectToAction("Pending", "Applications");

        }

        [HttpPost]
        public ActionResult Application(Application app, HttpPostedFileBase resume)
        {

            db.Applications.Add(app);
            db.SaveChanges();
            return ViewBag.Message("Thanks for applying!");

        }

        //GET: OpenPositions/Details/5
        //#endregion
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPosition openPosition = db.OpenPositions.Find(id);
            if (openPosition == null)
            {
                return HttpNotFound();
            }            

            return View(openPosition);
            
        }



        // GET: OpenPositions/Create
        public ActionResult Create()
        {
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber");
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title");
            return View();
        }

        // POST: OpenPositions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OpenPositionID,PositionID,LocationID")] OpenPosition openPosition)
        {
            if (ModelState.IsValid)
            {
                db.OpenPositions.Add(openPosition);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber", openPosition.LocationID);
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title", openPosition.PositionID);
            return View(openPosition);
        }

        // GET: OpenPositions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPosition openPosition = db.OpenPositions.Find(id);
            if (openPosition == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber", openPosition.LocationID);
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title", openPosition.PositionID);
            return View(openPosition);
        }

        // POST: OpenPositions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OpenPositionID,PositionID,LocationID")] OpenPosition openPosition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(openPosition).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "StoreNumber", openPosition.LocationID);
            ViewBag.PositionID = new SelectList(db.Positions, "PositionID", "Title", openPosition.PositionID);
            return View(openPosition);
        }

        // GET: OpenPositions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPosition openPosition = db.OpenPositions.Find(id);
            if (openPosition == null)
            {
                return HttpNotFound();
            }
            return View(openPosition);
        }

        // POST: OpenPositions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OpenPosition openPosition = db.OpenPositions.Find(id);
            db.OpenPositions.Remove(openPosition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}
