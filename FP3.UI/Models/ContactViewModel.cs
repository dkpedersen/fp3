﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FP3.Models
{
    public class ContactViewModel
    {

        [Required(ErrorMessage = "* Please enter your name")]
        public string Name
        {
            get; set;
        }

        [Required(ErrorMessage = "* Email is Required")]
        [EmailAddress(ErrorMessage = "Please enter a valid Email adress")]
        public string Email
        {
            get; set;
        }

        [Required(ErrorMessage = "* Please enter a subject")]
        public string Subject
        {
            get; set;
        }

        [Required(ErrorMessage = "* Please enter your message")]
        public string Message
        {
            get; set;
        }

    }
}