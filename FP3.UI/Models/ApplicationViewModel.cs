﻿using FP3.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FP3.UI.Models
{
    public class ApplicationViewModel
    {
        public int ApplicationID { get; set; }
        public int OpenPositionID { get; set; }
        public string UserID { get; set; }
        public System.DateTime ApplicationDate { get; set; }
        public string ManagerNotes { get; set; }
        public bool IsDeclined { get; set; }
        public string ResumeFilename { get; set; }
        public virtual OpenPosition OpenPosition { get; set; }
    }
}