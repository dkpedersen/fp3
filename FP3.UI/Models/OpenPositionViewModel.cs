﻿using FP3.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FP3.UI.Models
{
    public class OpenPositionViewModel
    {
        /// <summary>
        /// This viewmodel helps us search the titles collection.
        /// </summary>       
        public int OpenPositionID { get; set; }
        public int PositionID { get; set; }
        public int LocationID { get; set; }

    }
}
