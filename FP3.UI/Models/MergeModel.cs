﻿using FP3.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FP3.UI.Models
{
    public class MergeModel
    {
        public List<Application> app { get; set; }
        public List<UserDetail> user {get;set;}
    }
}